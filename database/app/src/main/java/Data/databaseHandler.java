package Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import Utilities.Util;
import model.Contact;

public class databaseHandler extends SQLiteOpenHelper {

    public databaseHandler(@Nullable Context context) {
        super(context, Util.DATABASE_NAME,null,Util.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String Create_Table = "CREATE TABLE " + Util.Table_Name + "("
                + Util.KEY_Id + " INTEGER PRIMARY KEY," + Util.KEY_NAME + " TEXT,"
                +Util.KEY_PHONE_NUMBER + "TEXT" + Util.KEY_CITY + "TEXT" + Util.KEY_FOOD + "TEXT" + ")";

        db.execSQL(Create_Table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Util.Table_Name);
        onCreate(db);
    }

    public void addContact(Contact contact){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues value = new ContentValues();
        value.put(Util.KEY_NAME, contact.getName());
        value.put(Util.KEY_PHONE_NUMBER, contact.getPhoneNumber());
        value.put(Util.KEY_CITY, contact.getCity());
        value.put(Util.KEY_FOOD, contact.getFood());

        db.insert(Util.Table_Name, null, value);
        db.close();
    }

    public Contact getContact(int id){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(Util.Table_Name, new String[]{Util.KEY_Id,
        Util.KEY_NAME, Util.KEY_PHONE_NUMBER, Util.KEY_CITY, Util.KEY_FOOD}, Util.KEY_Id + "=?",
                new String[] {String.valueOf(id)}, null,null,null);

        if(cursor != null)
            cursor.moveToFirst();

        Contact contact = new Contact(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1),cursor.getString(2));


        return contact;
    }

    public List<Contact> getAllContacts(){
        SQLiteDatabase db = this.getReadableDatabase();

        List<Contact> contactList = new ArrayList<>();

        String selectTheHouse = "SELECT * FROM " + Util.Table_Name;
        Cursor cursor = db.rawQuery(selectTheHouse, null);

        if (cursor.moveToFirst()){
            do{
               Contact contact = new Contact();
               contact.setId(Integer.parseInt(cursor.getString(0)));
               contact.setName(cursor.getString(1));
               contact.setPhoneNumber(cursor.getString(2));
                contact.setCity(cursor.getString(3));
                contact.setFood(cursor.getString(4));

               contactList.add(contact);

            }while (cursor.moveToNext());
        }
        return contactList;
    }
}
