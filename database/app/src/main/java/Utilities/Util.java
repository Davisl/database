package Utilities;

public class Util {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "contactsDB";
    public static final String Table_Name = "contacts";

    public static final String KEY_Id = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_PHONE_NUMBER = "phone_number";
    public static final String KEY_CITY = "city";
    public static final String KEY_FOOD = "food";


}
