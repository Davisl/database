package com.example.database;

import androidx.appcompat.app.AppCompatActivity;

import android.database.DatabaseErrorHandler;
import android.os.Bundle;
import android.util.Log;

import java.util.List;

import Data.databaseHandler;
import model.Contact;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        databaseHandler db = new databaseHandler(this);

        Log.d("Insert: ", "Inertin...");
        db.addContact(new Contact("AB", "4128448484", "homestead", "Apple"));
        db.addContact(new Contact("Crosby", "4120878787", "Munhall", "Orange"));
        db.addContact(new Contact("JoshBell", "4120550505", "Bradock", "Pizza"));
        db.addContact(new Contact("KevinLove", "4400000000", "Shadyside", "Cookie"));
        db.addContact(new Contact("Leland", "4126088748", "Oakland", "Cake"));

        Log.d("Reading: ", " Reading all contacts...");
        List<Contact> contactList = db.getAllContacts();

        for(Contact c : contactList){
            String log = "ID: " + c.getId()+" , Name: " + c.getName() + ", Phone: " + c.getPhoneNumber()
                    + ", City: " + c.getCity() + ", Food: " + c.getFood();
            Log.d("Name: ", log);
        }
    }
}
